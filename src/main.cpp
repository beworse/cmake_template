#include <iostream>
#include "adder.hpp"

int main(int argc, char *argv[])
{
    std::cout << "Hello World" << std::endl;

    // [-Wunused-parameter]
    std::cout << "Input parameters:\nargc: " 
        << argc << "\nargv:" << argv << std::endl;

    auto *tmp = new Adder();
    std::cout << "Result poiner: " << tmp->get(4,5) << std::endl;
    delete tmp;

    return 0;
}