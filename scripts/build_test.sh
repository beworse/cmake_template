#!/usr/bin/env bash

if [ -z "${PROJECT_DIR}" ] ; then
    echo "Please set variable \$\{PROJECT_DIR\}"
    exit 1
fi

export BUILD_DIR="${PROJECT_DIR}/build"
export COVERAGE_DIR="${BUILD_DIR}/coverage"

if [ -d ${BUILD_DIR} ] ; then
    rm -rfv ${BUILD_DIR}
fi

mkdir -pv ${BUILD_DIR}
cmake -S ${PROJECT_DIR} -B ${BUILD_DIR} -DBUILD_FOR_TESTS=1
cd ${BUILD_DIR} 
make

# Run UT
${BUILD_DIR}/tests

# generate report
cd ${BUILD_DIR}/CMakeFiles/tests.dir/tests/ut/
lcov --directory . --capture --output-file full.info 
# remove unwanted part from report
lcov --remove full.info "/usr/*" --output-file app.info
genhtml app.info -o ${COVERAGE_DIR}
echo "Coverage raprot generated ad ${COVERAGE_DIR}/index.html"