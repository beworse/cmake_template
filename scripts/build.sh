#!/usr/bin/env bash

if [ -z "${PROJECT_DIR}" ] ; then
    echo "Please set variable \$\{PROJECT_DIR\}"
    exit 1
fi

export BUILD_DIR="${PROJECT_DIR}/build"

if [ -d ${BUILD_DIR} ] ; then
    rm -rfv ${BUILD_DIR}
fi

cmake -S ${PROJECT_DIR} -B ${BUILD_DIR}
cd ${BUILD_DIR} 
make

echo -e "\nRunning app:"
${BUILD_DIR}/app