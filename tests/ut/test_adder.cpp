#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "adder.hpp"

TEST(adder, sum_test ){
    auto  *adder = new Adder();
    ASSERT_EQ(1+3, adder->get(1,3));
    delete adder;
}
